"use strict";

var gulp = require("gulp");

// html packages
var fileinclude = require("gulp-file-include");

// css packages
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var postcss = require("gulp-postcss");
var cssnano = require("cssnano");
var autoprefixer = require("autoprefixer");

// js packages
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");

// image packages
var imagemin = require("gulp-imagemin");

// server packages
var browserSync = require("browser-sync");
var php = require("gulp-connect-php");

// others
var clean = require("gulp-clean");
var plumber = require("gulp-plumber");


/* -----------------------------------
    Compile sass
------------------------------------- */

gulp.task("sass", function () {
  var plugins = [
    autoprefixer({browsers: ["last 50 version"]}),
    cssnano()
  ];
  return gulp.src("app/assets/scss/**/*.scss")
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("dist/assets/css"));
 });


/* -----------------------------------
    Concat JS in the given order
------------------------------------- */

gulp.task("scripts", function() {
  return gulp.src([

    // List in order the scripts you want to concat
    "app/assets/js/vendor/jquery-3.2.1.min.js",
    "app/assets/js/vendor/*.js",
    "app/assets/js/main.js"
    ])
    .pipe(plumber())
    // .pipe(sourcemaps.init())  // Uncomment if you want sourcemap on JS too 
    .pipe(concat("all.min.js"))
    .pipe(uglify())
    // .pipe(sourcemaps.write(".")) // Uncomment if you want sourcemap on JS too 
    .pipe(gulp.dest("dist/assets/js"));
});

// Running separately because it cannot be concatenated
gulp.task("scriptsPhp", function() {
  return gulp.src([
    "app/assets/js/vendor/*.php"
    ])
    .pipe(plumber())
    .pipe(gulp.dest("dist/assets/js"));
});



/* -----------------------------------
    Optimize images
------------------------------------- */

gulp.task("imagemin", function () {
  gulp.src(["app/assets/img/**/*","!app/assets/img/download/*"])
    .pipe(plumber())
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: false}),
      imagemin.jpegtran({progressive: false}),
      imagemin.optipng({optimizationLevel: 0}),
      ]
    ))
    .pipe(gulp.dest("dist/assets/img"))
});


/* -----------------------------------
    Copy php files
------------------------------------- */

gulp.task("php", function () {
  return gulp.src("app/**/*.php")
    .pipe(plumber())
    .pipe(gulp.dest("dist"))
});



/* -----------------------------------
    Just copy the download folder
------------------------------------- */

gulp.task("download", function () {
  gulp.src("app/assets/img/download/*")
    .pipe(plumber())
    .pipe(gulp.dest("dist/assets/img/download"))
});

/* -----------------------------------
    Just example json
------------------------------------- */

gulp.task("json", function () {
  gulp.src("app/**/*.json")
    .pipe(plumber())
    .pipe(gulp.dest("dist"))
});



/* -----------------------------------
    Compile, run server and watch for changes
------------------------------------- */

gulp.task("default", ["php", "sass", "scripts", "scriptsPhp","imagemin", "download", "json"], function() {

  var reload = browserSync.reload;

  php.server({
    base: 'dist', 
    port: 8020, 
    keepalive: true
  }, function (){
    browserSync({
      proxy: '127.0.0.1:8020',
      port: 8020,
      open: true,
      notify: false,
    });
  });
  
  gulp.watch(["app/**/*.php"],["php",reload]);
  gulp.watch(["app/**/*.json"],["json",reload]);
  gulp.watch(["app/assets/scss/**/*"], ["sass", reload]);
  gulp.watch(["app/assets/js/**/*"], ["scripts", reload]);
  gulp.watch(["app/assets/js/vendor/*.php"], ["scriptsPhp", reload]);
  gulp.watch(["app/assets/img/**/*"], ["imagemin", reload]);
  gulp.watch(["app/assets/img/download/*"], ["download", reload]);
  
});


/* -----------------------------------
    If necessary, run "gulp clean" 
    to clean img folder
------------------------------------- */

gulp.task("clean", function () {
  return gulp.src("dist", {read: false})
    .pipe(plumber())
    .pipe(clean());
});
