$(document).ready(function() {

  //---------------------------------
  // Smooth Scrolling
  //---------------------------------

  $('a[href*="#"]').click(function(){
    var the_id = $(this).attr("href").split("#")[1];
    the_id = "#" + the_id;
    $('html, body').animate({
      scrollTop:$(the_id).offset().top-50-$('.header').height()
    }, 'slow');

    return false;
  });

  if(window.location.hash.indexOf('#') != -1 ) {
    var section = window.location.hash;
    $('html, body').animate({
      scrollTop: $(section).offset().top-50-$('.header').height()
    }, "slow");
  }
  
});