<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php if (isset($page_title)) { echo $page_title;} ?></title>
  <meta name="author" content="">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/notosansjapanese.css">
  <link rel="stylesheet" href="/assets/css/main.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="">
  <link rel="icon" type="image/png" sizes="32x32" href="">
  <link rel="icon" type="image/png" sizes="16x16" href="">
  <link rel="manifest" href="">
  <link rel="mask-icon" href="" color="">
  <link rel="shortcut icon" href="">
  <meta name="msapplication-TileColor" content="">
  <meta name="msapplication-config" content="">
  <meta name="theme-color" content="">

  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="">
  <meta itemprop="description" content="">
  <meta itemprop="image" content="">

  <!-- Twitter Card data -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image:src" content="">

  <!-- Open Graph data -->
  <meta property="og:title" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta property="og:description" content="">
  <meta property="og:site_name" content="">

</head>
<body class="<?php if (isset($body_class)) {echo $body_class;} ?>">
  <div class="header">
    
  </div><!-- /.header -->
