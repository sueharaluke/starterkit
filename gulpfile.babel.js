//-----------------------
// List of packages
//-----------------------

import gulp from 'gulp';

// html packages
import fileinclude from 'gulp-file-include';

// css packages
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import postcss from 'gulp-postcss';
import cssnano from 'cssnano';
import autoprefixer from 'autoprefixer';

// js packages
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';

// image packages
import imagemin from 'gulp-imagemin';

// server packages
import browserSync from 'browser-sync';

// others
import clean from 'gulp-clean';
import plumber from 'gulp-plumber';
import newer from 'gulp-newer';


//-----------------------
// Custom paths
//-----------------------

const paths = {

  styles: {
    src: 'app/assets/scss/**/*.scss',
    dest: 'dist/assets/css'
  },

  scripts: {
    // Order of concatenation
    src: [
      'app/assets/js/vendor/jquery-3.2.1.min.js',
      'app/assets/js/vendor/*.js',
      'app/assets/js/main.js'
    ],
    dest: 'dist/assets/js'
  },

  images: {
    src: 'app/assets/img/**/*',
    dest: 'dist/assets/img'
  },

  html: {
    src: ['app/**/*.html', '!app/include/**/*.html'],
    includeBase: 'app/include',
    dest: 'dist'
  },

  server: 'dist'

}


//-----------------------
// Gulping around
//-----------------------

// scss compile
export function styles() {
  const plugins = [
    autoprefixer({browsers: ["last 50 version"]}),
    cssnano()
  ];
  return gulp.src(paths.styles.src)
    .pipe(plumber())
    .pipe(newer(paths.styles.dest))
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(paths.styles.dest));
}

// contact js
export function scripts() {
  return gulp.src(paths.scripts.src)
    .pipe(plumber())
    .pipe(newer(paths.scripts.dest))
    // .pipe(sourcemaps.init())  // Uncomment if you want sourcemap on JS too 
    .pipe(concat("all.min.js"))
    .pipe(uglify())
    // .pipe(sourcemaps.write(".")) // Uncomment if you want sourcemap on JS too 
    .pipe(gulp.dest(paths.scripts.dest));
}

// optimize images
export function images() {
  return gulp.src(paths.images.src)
    .pipe(plumber())
    .pipe(newer(paths.images.dest))
    .pipe(imagemin())
    .pipe(gulp.dest(paths.images.dest))
}

// include partials and copy html
export function html() {
  return gulp.src(paths.html.src)
    .pipe(plumber())
    .pipe(newer(paths.html.dest))
    .pipe(fileinclude({
      prefix: "@@",
      basepath: paths.html.includeBase
    }))
    .pipe(gulp.dest(paths.html.dest))
}

// run server
const createServer = browserSync.create();
export function server(done) {
  createServer.init({
    notify: false,
    server: {
      baseDir: paths.server
    }
  })
  done();
}

// reload server on changes
export function reload(done) {
  createServer.reload();
  done();
}

export function watch() {
  gulp.watch(paths.html.src, gulp.series(html, reload));
  gulp.watch(paths.styles.src, gulp.series(styles, reload));
  gulp.watch(paths.scripts.src, gulp.series(scripts, reload));
  gulp.watch(paths.images.src, gulp.series(images, reload));
}


const build = gulp.series(gulp.parallel(styles, scripts, images, html), server, watch)

export default build;