"use strict";

var gulp = require("gulp");

// html packages
var fileinclude = require("gulp-file-include");

// css packages
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var postcss = require("gulp-postcss");
var cssnano = require("cssnano");
var autoprefixer = require("autoprefixer");

// js packages
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");

// image packages
var imagemin = require("gulp-imagemin");

// server packages
var browserSync = require("browser-sync").create();

// others
var clean = require("gulp-clean");
var plumber = require("gulp-plumber");


/* -----------------------------------
    Compile sass
------------------------------------- */

gulp.task("sass", function () {
  var plugins = [
    autoprefixer({browsers: ["last 50 version"]}),
    cssnano()
  ];
  return gulp.src("app/assets/scss/**/*.scss")
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("dist/assets/css"));
 });


/* -----------------------------------
    Concat JS in the given order
------------------------------------- */

gulp.task("scripts", function() {
  return gulp.src([

    // List in order the scripts you want to concat
    "app/assets/js/vendor/jquery-3.2.1.min.js",
    "app/assets/js/vendor/*.js",
    "app/assets/js/main.js"

    ])
    .pipe(plumber())
    // .pipe(sourcemaps.init())  // Uncomment if you want sourcemap on JS too 
    .pipe(concat("all.min.js"))
    .pipe(uglify())
    // .pipe(sourcemaps.write(".")) // Uncomment if you want sourcemap on JS too 
    .pipe(gulp.dest("dist/assets/js"));
});



/* -----------------------------------
    Optimize images
------------------------------------- */

gulp.task("imagemin", function () {
  gulp.src("app/assets/img/**/*")
    .pipe(plumber())
    .pipe(imagemin())
    .pipe(gulp.dest("dist/assets/img"))
});



/* -----------------------------------
    Include partias and copy the HTML file
------------------------------------- */

gulp.task("html", function () {
  return gulp.src(["app/**/*.html", "!app/include/**/*.html"])
    .pipe(plumber())
    .pipe(fileinclude({
      prefix: "@@",
      basepath: "app/include"
    }))
    .pipe(gulp.dest("dist"))
});



/* -----------------------------------
    Compile, run server and watch for changes
------------------------------------- */

gulp.task("default", ["sass", "scripts", "imagemin", "html"], function() {

  var reload = browserSync.reload;
  browserSync.init({
    notify: false,
    server: {
      baseDir: "dist"
    }
  });

  gulp.watch(["app/**/*.html","app/include/**/*.html"], ["html", reload]);
  gulp.watch(["app/assets/scss/**/*"], ["sass", reload]);
  gulp.watch(["app/assets/js/**/*"], ["scripts", reload]);
  gulp.watch(["app/assets/img/**/*"], ["imagemin", reload]);

});


/* -----------------------------------
    If necessary, run "gulp clean" 
    to clean the dist folder
------------------------------------- */

gulp.task("clean", function () {
  return gulp.src("dist", {read: false})
    .pipe(plumber())
    .pipe(clean());
});
